---
title: "paper-figures"
author: "Marchetti Julia"
date: "10/4/2021"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
library(tidyverse)
library(patchwork)
library(GGally)
library(grid)
library(gridExtra)
library(gridtext)
library(ggpubr)
```

# Tabla de donde extraigo las categorías --> eliminar para todos los gráficos los mix y los que no tienen info

```{r message=FALSE, warning=FALSE}
final_8oct_tadeo <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/final_8oct_tadeo.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)
pdbs_category <- final_8oct_tadeo %>% 
  select(apo_id, holo_id, apo_method, holo_method, type) %>% 
  mutate(method = case_when(
              apo_method == 'X-ray diffraction' & holo_method == 'X-ray diffraction' ~ "X-ray",
              apo_method == 'X-ray diffraction' & holo_method == 'Solution NMR' ~ "mix",
              apo_method == 'Solution NMR' & holo_method == 'X-ray diffraction' ~ "mix", 
              apo_method == 'Solution NMR' & holo_method == 'Solution NMR' ~ "NMR",
              )
         ) %>% 
  rename(Apo_ID = apo_id, Holo_ID = holo_id)

rm(final_8oct_tadeo)
pdbs_category %>% 
  group_by(method) %>% 
  count()
```


# Figure 1

```{r}
# Set size
png(res = 300, width = 7, height = 5, units = "in")

# Read table
Apo_vs_Holo <- read_delim("../data/to_Juli/Table_rmsd_Apo_vs_Holo.csv", ";", escape_double = FALSE, trim_ws = TRUE) 

Apo_vs_Holo_filtrada <- Apo_vs_Holo %>% 
  left_join(pdbs_category) %>% 
  filter(method == 'NMR' | method == 'X-ray')


figure1 <-  
    ggplot() +
  
    # Capa de distribución general
    geom_density(data = Apo_vs_Holo,
                 aes(x = RMSD,y = ..count../sum(..count..)), 
                 alpha = 0.3, color = 'orange',
                 #fill = 'orange',
                 adjust = 1) + 
  
   # Capa de distribución por método
   geom_density(data = Apo_vs_Holo_filtrada, 
                 aes(x = RMSD,y = ..count../sum(..count..),color = method, fill = method), 
                alpha = 0.3,
                adjust = 1) + 
    

  
  xlab(expression(RMSD[Apo~vs~Holo]~(Å))) + ylab('Density') +
  
  scale_x_continuous(limits = c(0,15.5),breaks=seq(0, 15, 1), expand = c(0,0)) +
  scale_y_continuous(limits = c(0,0.009), breaks = seq(0.000, 0.009, 0.001), expand = c(0,0)) +
  
  theme(legend.position = "none") +
  theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 10))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

# Make plot
ggsave("../figures/fig1.png", plot = figure1)
dev.off()



figure1
```
### Estadística

```{r}
Apo_vs_Holo_filtrada %>% 
  filter(method == 'NMR') %>% 
  summary()

Apo_vs_Holo_filtrada %>% 
  filter(method == 'X-ray') %>% 
  summary()
#Remove table
rm(Apo_vs_Holo)
rm(Apo_vs_Holo_filtrada)
```



# Figure 2

## Panel a)

This figure originally is done with Table_rmsd_vs_model-OK.csv, x = RMSD_vs_Apo , y = RMSD_vs_Holo.
In table final_4_october RMSD_vs_Apo --> 

```{r}
models_vs_Apo_Holo <- read_delim("../data/to_Juli/Table_rmsd_vs_model-OK.csv", ";", escape_double = FALSE, trim_ws = TRUE)


figure2a <- models_vs_Apo_Holo %>% 
  ggplot(aes(x = RMSD_vs_Apo, y = RMSD_vs_Holo)) +
  geom_point(size = 2, show.legend = TRUE, alpha = 0.3,color = 'orange', fill= 'orange') +
  labs(x=expression(RMSD[~vs~Apo]~(Å)),y=expression(RMSD[~vs~Holo]~(Å)))  +
  
  geom_abline(intercept = 0, slope = 1,linetype="dashed") +
  scale_x_continuous(breaks=0:18*2, limits=c(0,14),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*2,limits=c(0,14.2),expand=c(0,0)) +
  
    theme(
      axis.line = element_line(colour = "black"),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.border = element_blank(),
      panel.background = element_blank()) +
   
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 20, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 20, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 13))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  
  ggtitle('A')+
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))
  

rm(models_vs_Apo_Holo)
figure2a
```



## panel b)

```{r message=FALSE, warning=FALSE}
models_vs_Apo_Holo_best <- read_delim("../data/to_Juli/Table_rmsd_vs_best_model-OK.csv", ";", escape_double = FALSE, trim_ws = TRUE) 


figure2b <- models_vs_Apo_Holo_best %>%  
ggplot(aes(x = RMSD_vs_Apo, y = RMSD_vs_Holo)) + 
  geom_point(size= 2, show.legend = TRUE, alpha = 0.3,color = 'orange', fill= 'orange') +
  labs(x=expression(RMSD[~vs~Apo]~(Å)),y = expression(RMSD[~vs~Holo]~(Å))) + 
  geom_abline(intercept = 0, slope = 1,linetype ="dashed") +
  scale_x_continuous(breaks=0:18*2, limits =c(0,14),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*2,limits =c(0,14.2),expand=c(0,0))+
  
  
    theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
   theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 20, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 20, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 13))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  ggtitle('B')+
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

rm(models_vs_Apo_Holo_best)
figure2b
```

## Save figure
```{r}
# Set size
png(res = 300, width = 14, height = 8, units = "in")

# Make plot 
figure2 <- figure2a + figure2b

# Save plot
ggsave("../figures/fig2.png", plot = figure2)
dev.off()
```


# Figure 3 de relación e/ rmsd apo - holo par a par

```{r}

Table_lowest_compare_vs_Apo_vs_Holo <- read_delim("../data/to_Juli/Table_lowest_compare_vs_Apo_vs_Holo.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

compare_pairs <- list(c("RMSD_vs_Apo", "RMSD_vs_Holo"))

figure3 <- Table_lowest_compare_vs_Apo_vs_Holo <- Table_lowest_compare_vs_Apo_vs_Holo %>% 
  left_join(pdbs_category) %>% 
  filter(method == 'NMR' | method == 'X-ray') %>% 
  pivot_longer(cols = c("RMSD_vs_Apo", "RMSD_vs_Holo"), 
               names_to = "RMSD_apo_holo", 
               values_to = "RMSD_lowest_value") %>% 
  
  ggboxplot(x = 'RMSD_apo_holo', y = "RMSD_lowest_value", facet.by = "type")+
  stat_compare_means(comparisons = compare_pairs,label = "p.format", method = "wilcox.test") +
  xlab('') + ylab(expression(lowest~RMSD~(Å)))
  
figure3
rm(Table_lowest_compare_vs_Apo_vs_Holo)

# Set size
png(res = 300, width = 10, height = 7, units = "in")

# Save plot
ggsave("../figures/fig3.png", plot = figure3)
dev.off()
```





# Figure 5 (old figure 4)

## Panel a - tables

```{r message=FALSE, warning=FALSE}
Table_lower_RMSD_of_all_vs_ApoHolo_method_XR <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/Table_lower_RMSD_of_all_vs_ApoHolo_method_XR.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

Table_lower_RMSD_of_all_vs_ApoHolo_method_NMR <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/Table_lower_RMSD_of_all_vs_ApoHolo_method_NMR.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

table_lower_RMSD_of_all_vs_ApoHolo <- bind_rows(Table_lower_RMSD_of_all_vs_ApoHolo_method_XR,
                                                Table_lower_RMSD_of_all_vs_ApoHolo_method_NMR)

rm(Table_lower_RMSD_of_all_vs_ApoHolo_method_XR)
rm(Table_lower_RMSD_of_all_vs_ApoHolo_method_NMR)
```

## Panel a - figures
```{r}
figure5a <-table_lower_RMSD_of_all_vs_ApoHolo %>% 
  ggplot(aes(x = RMSD_Apo_vs_Holo, y = RMSD_vs_Model))+
  geom_point(aes(color=Method, fill=Method), alpha=0.7) +
  
  geom_smooth(aes(fill=Method, color= Method),method = "lm") +
  
  labs(x=expression(RMSD[Apo~vs~Holo]~(Å)),y=expression(lowest~RMSD~(Å))) + 
  scale_x_continuous(breaks=0:18*1, limits=c(0.5,15),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*1,limits=c(0,14),expand=c(0,0))+
      
  theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
  theme(legend.position = 'none') +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 20, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 20, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 10))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
    scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))+
  ggtitle('A')

figure5a
```

```{r}
xray <- table_lower_RMSD_of_all_vs_ApoHolo %>% filter(Method == 'X-ray_diffraction')
cor.test(xray$RMSD_vs_Model, xray$RMSD_Apo_vs_Holo)

nmr <- table_lower_RMSD_of_all_vs_ApoHolo %>% filter(Method == 'Solution_NMR')
cor.test(nmr$RMSD_vs_Model, nmr$RMSD_Apo_vs_Holo)

rm(xray, nmr)
```


## Panel B - tables

```{r message=FALSE, warning=FALSE}
tabla_Score_Global_best_model_method_XR <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/tabla_Score_Global_best_model_method_XR.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)
tabla_Score_Global_best_model_method_NMR <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/tabla_Score_Global_best_model_method_NMR.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

tabla_Score_Global_best_model_method <- rbind(tabla_Score_Global_best_model_method_XR,tabla_Score_Global_best_model_method_NMR) %>% 
  left_join(table_lower_RMSD_of_all_vs_ApoHolo %>% select(apo_id,holo_id,RMSD_Apo_vs_Holo))

#rm(tabla_Score_Global_best_model_method_XR)
#rm(tabla_Score_Global_best_model_method_NMR)
```


## Panel B - figure
```{r}
figure5b <- ggplot()+
  geom_point(aes(#x = table_lower_RMSD_of_all_vs_ApoHolo$RMSD_Apo_vs_Holo,
                  x = tabla_Score_Global_best_model_method$RMSD_Apo_vs_Holo,
                 y = tabla_Score_Global_best_model_method$score,
                 color=tabla_Score_Global_best_model_method$Method, 
                 fill= tabla_Score_Global_best_model_method$Method))+
  
  geom_smooth(aes(#x = table_lower_RMSD_of_all_vs_ApoHolo$RMSD_Apo_vs_Holo, 
    x = tabla_Score_Global_best_model_method$RMSD_Apo_vs_Holo,
                  y = tabla_Score_Global_best_model_method$score,
                  color=tabla_Score_Global_best_model_method$Method, 
                 fill= tabla_Score_Global_best_model_method$Method),method = "lm") +
  
  labs(x=expression(RMSD[Apo~vs~Holo]~(Å)),y=expression(plDDT[Score])) + 
  
  
  
  scale_x_continuous(limits=c(0.5,15.2), breaks=0:18*1,expand=c(0,0))+
  scale_y_continuous(limits=c(75,100.0),expand=c(0,0))+

  theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
  theme(legend.position = 'none') +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 20, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 20, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 10))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))+
  
  ggtitle('B')
figure5b
```

## Save figure
```{r}
# Set size
png(res = 300, width = 14, height = 8, units = "in")

# Make plot 
figure5 <- figure5a + figure5b
figure5
# Save plot
ggsave("../figures/fig5.png", plot = figure5)
dev.off()

rm(tabla_Score_Global_best_model_method)
rm(table_lower_RMSD_of_all_vs_ApoHolo)
```



# Figure 6 - rmsd clusters (ex-figure5)
## Read tables
```{r message=FALSE, warning=FALSE}
clusters_maxRMSD_stats_Xray_ <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/clusters_maxRMSD_stats(Xray).csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

clusters <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/clusters.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)
clusters <- clusters %>% 
  mutate(Disperse = ifelse(`Dispersa (Sigma>2)` == 1,'heterogeneus','homogeneus')) %>% 
  mutate(Rigid = ifelse(`Rigida (RMSD<1.0)` == 1,'rigid', 'not rigid'))

X196_lowestRmsd_with_method_maxs <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/196_lowestRmsd_with_method_maxs.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

X196_lowestRmsd_with_method_maxs <- X196_lowestRmsd_with_method_maxs %>% 
  mutate(cluster = paste('Clus',Cluster_ID, sep = "")) %>% 
  filter(methods == 'X-RAY DIFFRACTION..X-RAY DIFFRACTION')
```

## Define categories (rigid/dispersion)
```{r}
clusters_maxRMSD_stats_Xray_ <- clusters_maxRMSD_stats_Xray_ %>% 
  mutate(rango = abs(max-min)) %>% 
  mutate(dispersion_range = ifelse(rango >= 4, 'heterogeneous', 'homogeneous')) %>% 
  mutate(dispersion = ifelse(std >2, 'heterogeneous', 'homogeneous')) %>% 
  mutate(rigida = ifelse(mean < 1, 'rigid', 'flexible')) %>% 
  filter(count >= 8) 
```


## Merge two tables
```{r}
figure6_table <- X196_lowestRmsd_with_method_maxs %>% 
  left_join(clusters_maxRMSD_stats_Xray_)

```

## Panel a -- dispersion
```{r}
compare_pairs <- list(c('heterogeneous','homogeneous'))

figure6a <- figure6_table %>% 
  drop_na(dispersion_range) %>% 
  ggboxplot(x = 'dispersion_range', y = "RMSD_Mammoth")+
  xlab('') + ylab(expression(lowest~RMSD~(Å)))+
  ggtitle('A')+
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
 stat_compare_means(comparisons = compare_pairs,label = "p.format", method = "wilcox.test")
figure6a
```

## Panel A -- Estadística
```{r}
homo <- figure6_table %>% filter(dispersion_range == 'homogeneous')
hetero <- figure6_table %>% filter(dispersion_range == 'heterogeneous')
summary(homo$RMSD_Mammoth)
summary(hetero$RMSD_Mammoth)

wilcox.test(homo$RMSD_Mammoth, hetero$RMSD_Mammoth)
rm(homo, hetero)
```

## Panel B -- rigid and flexible
```{r}
compare_pairs <- list(c('rigid','flexible'))

figure6b <-figure6_table %>% 
  drop_na(rigida) %>% 
  ggboxplot(x = 'rigida', y = "RMSD_Mammoth")+
  xlab('') + ylab('')+
  ggtitle('B')+
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  stat_compare_means(comparisons = compare_pairs,label = "p.format", method = "wilcox.test")
figure6b
```

## Panel B - Comparaisons
```{r}
rigid <- figure6_table %>% filter(rigida == 'rigid')
flexible <- figure6_table %>% filter(rigida == 'flexible')

summary(rigid$RMSD_Mammoth)
summary(flexible$RMSD_Mammoth)

wilcox.test(rigid$RMSD_Mammoth, flexible$RMSD_Mammoth)
rm(rigid, flexible)
```

## Make figure
```{r}
# Set size
png(res = 300, width = 14, height = 8, units = "in")

# Make plot 
figure6 <- figure6a + figure6b
figure6
# Save plot
ggsave("../figures/fig6.png", plot = figure6)
dev.off()

rm(X196_lowestRmsd_with_method_maxs)
rm(figure6_table)
```


# Figure 7 --> ex figure 6

## Tidy tables
### RMSF vs best model all and 15 window
```{r message=FALSE, warning=FALSE}
rmsf_bestModel_methods <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/rmsf_bestModel_methods.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

 rmsf_bestModel_methods <- rmsf_bestModel_methods %>% 
   mutate(method = case_when(
              apo_method == 'X-ray diffraction' & holo_method == 'X-ray diffraction' ~ "X-ray",
              apo_method == 'Solution NMR' & holo_method == 'Solution NMR' ~ "NMR",
              )
         ) %>% 
   filter(method == 'NMR' | method == 'X-ray')
 
 
xray_window_15_rmsf_vs_bestModel <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/xray_window_15_rmsf_vs_bestModel.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  mutate(method = 'X-ray')

nmr_window_15_rmsf_vs_bestModel <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/nmr_window_15_rmsf_vs_bestModel.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  mutate(method = 'NMR')


window_15_rmsf_vs_bestModel <- rbind(xray_window_15_rmsf_vs_bestModel,nmr_window_15_rmsf_vs_bestModel)

rm(xray_window_15_rmsf_vs_bestModel,nmr_window_15_rmsf_vs_bestModel)
```

```{r}
rmsf_bestModel_methods %>% 
  select(method, Apo_ID, Holo_ID) %>% 
  distinct() %>% 
  group_by(method) %>% 
  count(n())
```


### RMSF vs lowest RMSD model all and 15 window
```{r message=FALSE, warning=FALSE}

rmsf_lowest_methods <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/rmsf_lowest_methods.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
   mutate(method = case_when(
              apo_method == 'X-ray diffraction' & holo_method == 'X-ray diffraction' ~ "X-ray",
              apo_method == 'Solution NMR' & holo_method == 'Solution NMR' ~ "NMR",
              )
         ) %>% 
   filter(method == 'NMR' | method == 'X-ray')

xray_window_15_rmsf_vs_lowest <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/xray_window_15_rmsf_vs_lowest.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  mutate(method = 'Xray')

nmr_window_15_rmsf_vs_lowest <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/nmr_window_15_rmsf_vs_lowest.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  mutate(method = 'NMR')

window_15_rmsf_vs_lowestModel <- rbind(xray_window_15_rmsf_vs_lowest,nmr_window_15_rmsf_vs_lowest)
rm(xray_window_15_rmsf_vs_lowest,nmr_window_15_rmsf_vs_lowest)

```

```{r}
rmsf_lowest_methods %>% 
  select(method, apo_id, Holo_ID) %>% 
  distinct() %>% 
  group_by(method) %>% 
  count(n())
```


## Panel A - left - Best global Score - all residues

```{r}
figure7a_left <- rmsf_bestModel_methods %>%  
  ggplot()+
  geom_point(aes(x = RMSF_CA, y = Score_Best_M, fill=method, color = method),shape=16,alpha=0.3, size = 3) +
  ggtitle('All residues') +
  geom_smooth(aes(x = RMSF_CA, y = Score_Best_M, fill= method, color = method),method = "lm")+
  
          theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
   theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  theme(plot.title = element_text(hjust=0.5, size = 10)) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

figure7a_left
```


## Panel A - right - Best global Score - window 15 residues

```{r}
figure7a_right <- window_15_rmsf_vs_bestModel %>%  
  ggplot()+
  geom_point(aes(x = rmsf, y = scores, fill=method, color = method),shape=16,alpha=0.3, size = 3) +
  ggtitle('window 15 residues') +
  geom_smooth(aes(x = rmsf, y = scores, fill= method, color = method),method = "lm")+
  
          theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
   theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  theme(plot.title = element_text(hjust=0.5, size = 10)) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

figure7a_right
```

```{r}
figure7a <- figure7a_left + figure7a_right

png(res = 300, width = 10, height = 5, units = "in")

# Save plot
ggsave("../figures/fig7a.png", plot = figure7a)
dev.off()
```



## Panel B - Lowest RMSD - all residues

```{r}
figure7b_left <- rmsf_lowest_methods %>% 
  
   ggplot()+
  geom_point(aes(x = RMSF_CA, y = Score_Lower_rmsd_M, fill=method, color = method),shape=16,alpha=0.3, size = 3) +
  geom_smooth(aes(x = RMSF_CA, y = Score_Lower_rmsd_M, fill=method, color = method),method = "lm")+
  ggtitle('All residues') +
    theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
   theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  theme(plot.title = element_text(hjust=0.5, size = 10)) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

figure7b_left
```


## Panel B - Lowest RMSD - every 15 residues

```{r}
figure7b_right <- window_15_rmsf_vs_lowestModel %>% 
  
   ggplot()+
  geom_point(aes(x = rmsf, y = scores, fill=method, color = method),shape=16,alpha=0.3, size = 3) +
  geom_smooth(aes(x = rmsf, y = scores, fill=method, color = method),method = "lm")+
  ggtitle('window 15 residues') +
    theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
   theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  theme(plot.title = element_text(hjust=0.5, size = 10)) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

figure7b_right
```

```{r}
figure7b <- figure7b_left + figure7b_right

png(res = 300, width = 10, height = 5, units = "in")

# Save plot
ggsave("../figures/fig7b.png", plot = figure7b)
dev.off()
```



## Figure 6a + 6b

```{r}

#figure6a<-gridExtra::grid.arrange(figure6a_left, figure6a_rigth,
 #                       nrow = 1 , 
  #                      left = textGrob((expression(plDDT[Score])), rot=90, vjust = 1), 
   #                     #bottom = textGrob(expression(RMSF[Apo~vs~Holo]~(Å))), 
    #                    top = textGrob('A: Best global Score', hjust=2.5,
     #                                  gp=gpar(fontsize=14, fontface="bold"))
 #                       )
#figure6a

# figure6b<-gridExtra::grid.arrange(figure6b_left, figure6b_rigth,
#                         nrow = 1 , 
#                         left = textGrob(expression(plDDT[Score]), rot=90, vjust = 1), 
#                         bottom = textGrob(expression(RMSF[Apo~vs~Holo]~(Å))), 
#                         top = textGrob('B: Lowest RMSD model', hjust=2,
#                                        gp=gpar(fontsize=14, fontface="bold"))
#                         )
# figure6b

figure6<-gridExtra::grid.arrange(figure6a, figure6b, nrow=2)
figure6
```


# S1

## Panel A - Global Score

```{r}
tabla_Score_Global_method <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/tabla_Score_Global_method.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  mutate(method = ifelse(Method_Holo == 'X-ray_diffraction','X-ray','NMR'))

figureS1_a <- tabla_Score_Global_method %>% 
  
    ggplot() +
  
   geom_density(aes(x = score,y = ..count../sum(..count..),color = method, fill = method), 
                alpha = 0.3,
                adjust = 1) +
    labs(x="LDDT global score",y=expression(Density))+
  ggtitle('A: Global Scores')+
    #theme(legend.position = "none") +
  theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 10))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

figureS1_a
#rm(tabla_Score_Global_method)
```


## Panel B - Best Score
```{r}


# Read table
# Xray

tabla_Score_Global_best_model_method_XR <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/tabla_Score_Global_best_model_method_XR.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  mutate(method = 'X-ray')

# NMR
tabla_Score_Global_best_model_method_NMR <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/tabla_Score_Global_best_model_method_NMR.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  mutate(method = 'NMR')

tabla_Score_Global_best_model_method <- rbind(tabla_Score_Global_best_model_method_XR,
                                              tabla_Score_Global_best_model_method_NMR)

rm(tabla_Score_Global_best_model_method_XR)
rm(tabla_Score_Global_best_model_method_NMR)

figureS1_b <- tabla_Score_Global_best_model_method %>% 
  filter(score >= 86) %>% 
  
    ggplot() +
  #geom_boxplot(aes(y=score, color = method))+
   geom_density(aes(x = score,y = ..count../sum(..count..),color = method, fill = method), 
                alpha = 0.3,
                adjust = 1) +
    labs(x="LDDT global score",y=expression(Density))+
  ggtitle('B: Best model Score')+
    #theme(legend.position = "none") +
  scale_x_continuous(limits = c(0,100),breaks=seq(0, 100, 10), expand = c(0,0)) +
  
  theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  
  theme(axis.title.x = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 10))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

figureS1_b
```

## Make plot
```{r}
figureS1 <- figureS1_a / figureS1_b

png(res = 300, width = 7, height = 10, units = "in")

# Save plot
ggsave("../figures/figS1.png", plot = figureS1)
dev.off()
```

## Figura S1 - lowest RMSD
```{r message=FALSE, warning=FALSE}
Table_lower_RMSD_of_all_vs_ApoHolo_method_XR <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/Table_lower_RMSD_of_all_vs_ApoHolo_method_XR.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

Table_lower_RMSD_of_all_vs_ApoHolo_method_NMR <- read_delim("~/Documents/SBG/af2confdiv-oct2021/data/Table_lower_RMSD_of_all_vs_ApoHolo_method_NMR.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

table_lower_RMSD_of_all_vs_ApoHolo <- bind_rows(Table_lower_RMSD_of_all_vs_ApoHolo_method_XR,
                                                Table_lower_RMSD_of_all_vs_ApoHolo_method_NMR)

rm(Table_lower_RMSD_of_all_vs_ApoHolo_method_XR)
rm(Table_lower_RMSD_of_all_vs_ApoHolo_method_NMR)

table_lower_RMSD_of_all_vs_ApoHolo %>% 
  
    ggplot() +
  
   geom_density(aes(x = RMSD_vs_Model,y = ..count../sum(..count..),color = Method, fill = Method), 
                alpha = 0.3,
                adjust = 1) +
    labs(x="lowest RMSD",y=expression(Density))+
    #theme(legend.position = "none") +
  theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 10))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

```

```{r}
prueba <- table_lower_RMSD_of_all_vs_ApoHolo %>% 
  select(-Model_ID) %>% 
  left_join(tabla_Score_Global_best_model_method %>%  select(-Model_ID))

prueba %>% 
  ggplot()+
  geom_point(aes(x=RMSD_vs_Model, y = score, fill=Method, color = Method))+
    geom_smooth(aes(x = RMSD_vs_Model, y = score, fill=Method, color = Method),method = "lm")+
    scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))
```
```{r}
cor.test(prueba$RMSD_vs_Model, prueba$score)
```

```{r}
nmr <- prueba %>%  filter(method == 'NMR')
cor.test(nmr$RMSD_vs_Model, nmr$score)
```

```{r}
xray <- prueba %>%  filter(method == 'X-ray')
cor.test(xray$RMSD_vs_Model, xray$score)
```



# Figuras viejas

## Figura 1 - sin la distribución global

```{r}
# Set size
png(res = 300, width = 7, height = 5, units = "in")

# Read table
Apo_vs_Holo <- read_delim("../data/to_Juli/Table_rmsd_Apo_vs_Holo.csv", ";", escape_double = FALSE, trim_ws = TRUE) 

Apo_vs_Holo <- Apo_vs_Holo %>% 
  left_join(pdbs_category) %>% 
  filter(method == 'NMR' | method == 'X-ray')

figure1 <- Apo_vs_Holo %>% 
    ggplot(aes(fill = method, color = method)) +
    geom_density(aes(x = RMSD,y = ..count../sum(..count..)), adjust = 1/3, alpha = 0.3) + 
  xlab(expression(max~RMSD~(Å))) + ylab('Density') +
  scale_x_continuous(limits=c(0,15.5),breaks=seq(0, 15, 1), expand=c(0,0))+
  scale_y_continuous(limits=c(0,0.009), breaks = seq(0.000, 0.009, 0.001), expand = c(0,0)) +
  #theme(legend.position="none") +
  theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 10))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

# Make plot
ggsave("../figures/fig1.png", plot = figure1)
dev.off()

# Remove table
rm(Apo_vs_Holo)

figure1
```

## figura 2 --> separda

### panel A
```{r}
models_vs_Apo_Holo <- read_delim("../data/to_Juli/Table_rmsd_vs_model-OK.csv", ";", escape_double = FALSE, trim_ws = TRUE)

models_vs_Apo_Holo <- models_vs_Apo_Holo %>%  
  left_join(pdbs_category) %>% 
  filter(method == 'NMR' | method == 'X-ray')

figure2a <- models_vs_Apo_Holo %>% 
  ggplot(aes(x = RMSD_vs_Apo, y = RMSD_vs_Holo, colour = method)) +
  geom_point(size = 2, show.legend = TRUE, alpha = 0.3) +
  labs(x=expression(RMSD[~vs~Apo]~(Å)),y=expression(RMSD[~vs~Holo]~(Å)))  +
  
  geom_abline(intercept = 0, slope = 1,linetype="dashed") +
  scale_x_continuous(breaks=0:18*2, limits=c(0,14),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*2,limits=c(0,14.2),expand=c(0,0)) +
  
    theme(
      axis.line = element_line(colour = "black"),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.border = element_blank(),
      panel.background = element_blank()) +
   
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 20, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 20, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 13))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  
  ggtitle('A')+
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))
  

rm(models_vs_Apo_Holo)
figure2a
```



## panel b)

```{r message=FALSE, warning=FALSE}
models_vs_Apo_Holo_best <- read_delim("../data/to_Juli/Table_rmsd_vs_best_model-OK.csv", ";", escape_double = FALSE, trim_ws = TRUE) 

models_vs_Apo_Holo_best <- models_vs_Apo_Holo_best %>%  
  left_join(pdbs_category) %>% 
  filter(method == 'NMR' | method == 'X-ray')


figure2b <- models_vs_Apo_Holo_best %>%  
ggplot(aes(x = RMSD_vs_Apo, y = RMSD_vs_Holo, color= method, fill = method)) + 
  geom_point(size= 2, show.legend = TRUE, alpha = 0.3) +
  labs(x=expression(RMSD[~vs~Apo]~(Å)),y = expression(RMSD[~vs~Holo]~(Å))) + 
  geom_abline(intercept = 0, slope = 1,linetype ="dashed") +
  scale_x_continuous(breaks=0:18*2, limits =c(0,14),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*2,limits =c(0,14.2),expand=c(0,0))+
  
  
    theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
   theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 20, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 20, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 13))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  ggtitle('B')+
  
  scale_fill_manual(values=c("#D7191c","#2c7bb6"))+
  scale_color_manual(values=c("#D7191c","#2c7bb6"))

rm(models_vs_Apo_Holo_best)
figure2b
```


```{r}
# Set size
png(res = 300, width = 14, height = 8, units = "in")

# Make plot 
figure2 <- figure2a + figure2b

# Save plot
ggsave("../figures/fig2.png", plot = figure2)
dev.off()

```


 