
library(ggplot2)
library(readr)
library(gridExtra)

# Figure 1 -  RMSD Apo vs Holo  
Apo_vs_Holo <- read_delim("RMSD/Table_rmsd_Apo_vs_Holo.csv", ";", escape_double = FALSE, trim_ws = TRUE)
View(Apo_vs_Holo)

vec2 <- data.frame(x=Apo_vs_Holo$RMSD)

ggplot() + 
  geom_density(aes(x=x,y = ..count../sum(..count..)), adjust = 1/3, color="black", fill="blue", alpha = 1.0, data=vec2) + 
  theme_bw()+
  labs(x="RMSD (Å)",y=expression(Density)) +
  theme( legend.position="none",text=element_text(family="Times New Roman") , axis.title.y = element_text(size = 54,vjust=1.0), axis.text.y = element_text(size = 44,colour="black"), axis.title.x = element_text(size = 54,vjust=1.0), axis.text.x = element_text(size = 44,colour="black"), panel.grid.minor=element_blank(), panel.grid.major=element_blank(), plot.margin=unit(c(0.4,0.2,0.2,0.2), "cm"), axis.ticks = element_line(size=1.5,colour="black"),axis.ticks.length=unit(0.2, "cm")) +
  scale_x_continuous(limits=c(0.2,14.9),breaks=0:18*1, expand=c(0,0)) +
  #scale_y_continuous(breaks=0:5*0.002,limits=c(0,0.0062),expand=c(0,0))
  #scale_y_continuous(breaks=0:5*0.0025,limits=c(0,0.0078),expand=c(0,0))
  scale_y_continuous(breaks=0:5*0.0025,limits=c(0,0.0092),expand=c(0,0))


# Figure 2 a - RMSD Apo vs RMSD Holo all models ####
models_vs_Apo_Holo <- read_delim("RMSD/Table_rmsd_vs_model-OK.csv", ";", escape_double = FALSE, trim_ws = TRUE)
ggplot(models_vs_Apo_Holo, aes(x = RMSD_vs_Apo, y = RMSD_vs_Holo)) + 
  geom_point(size=3, show.legend = FALSE) +
  labs(x=expression(RMSD[vsApo]~(Å)),y=expression(RMSD[vsHolo]~(Å))) + 
  theme_bw() +
  theme(legend.position="none",text=element_text(family="Times New Roman") , axis.title.y = element_text(size = 54,vjust=1.0), axis.text.y = element_text(size = 44,colour="black"), axis.title.x = element_text(size = 54,vjust=1.0), axis.text.x = element_text(size = 44,colour="black"), panel.grid.minor=element_line(colour = "dark grey"), panel.grid.major=element_line(colour = "dark grey"), plot.margin=unit(c(0.4,0.4,0.2,0.2), "cm"), axis.ticks = element_line(size=1.5,colour="black"),axis.ticks.length=unit(0.2, "cm")) + 
  geom_abline(intercept = 0, slope = 1,linetype="dashed") +
  #scale_x_continuous(breaks=0:18*2, limits=c(0,14.2),expand=c(0,0))+
  scale_x_continuous(breaks=0:18*2, limits=c(0,9.2),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*2,limits=c(0,14.2),expand=c(0,0))
#scale_y_continuous(breaks=0:18*2,limits=c(0,9.5),expand=c(0,0))

# Figure 2 b - RMSD Apo vs RMSD Holo best models ####
models_vs_Apo_Holo <- read_delim("RMSD/Table_rmsd_vs_best_model-OK.csv", ";", escape_double = FALSE, trim_ws = TRUE)
ggplot(models_vs_Apo_Holo, aes(x = RMSD_vs_Apo, y = RMSD_vs_Holo)) + 
  geom_point(size=3, show.legend = FALSE) +
  labs(x=expression(RMSD[vsApo]~(Å)),y=expression(RMSD[vsHolo]~(Å))) + 
  theme_bw() +
  theme(legend.position="none",text=element_text(family="Times New Roman") , axis.title.y = element_text(size = 54,vjust=1.0), axis.text.y = element_text(size = 44,colour="black"), axis.title.x = element_text(size = 54,vjust=1.0), axis.text.x = element_text(size = 44,colour="black"), panel.grid.minor=element_line(colour = "dark grey"), panel.grid.major=element_line(colour = "dark grey"), plot.margin=unit(c(0.4,0.4,0.2,0.2), "cm"), axis.ticks = element_line(size=1.5,colour="black"),axis.ticks.length=unit(0.2, "cm")) + 
  geom_abline(intercept = 0, slope = 1,linetype="dashed") +
  #scale_x_continuous(breaks=0:18*2, limits=c(0,14.2),expand=c(0,0))+
  scale_x_continuous(breaks=0:18*2, limits=c(0,9.2),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*2,limits=c(0,14.2),expand=c(0,0))
#scale_y_continuous(breaks=0:18*2,limits=c(0,9.5),expand=c(0,0))

# Figure 4 a ###
models_vs_Apo_Holo1 <- read_delim("RMSD/Table_lower_RMSD_of_all_vs_ApoHolo.csv", ";", escape_double = FALSE, trim_ws = TRUE)

ggplot() + 
  geom_point(aes(x = models_vs_Apo_Holo1$RMSD_Apo_vs_Holo, y = models_vs_Apo_Holo1$RMSD_vs_Model),color="black",shape=15,size=4,alpha=0.7) +
  geom_smooth(aes(x = models_vs_Apo_Holo1$RMSD_Apo_vs_Holo, y = models_vs_Apo_Holo1$RMSD_vs_Model),method = "lm", color="black") +
  labs(x=expression(RMSD[ApovsHolo]~(Å)),y=expression(lower~RMSD[Modelvs]~(Å))) + 
  theme_bw() +
  theme(legend.position="none",text=element_text(family="Times New Roman") , axis.title.y = element_text(size = 44,vjust=1.0), axis.text.y = element_text(size = 44,colour="black"), axis.title.x = element_text(size = 54,vjust=1.0), axis.text.x = element_text(size = 44,colour="black"), panel.grid.minor=element_line(colour = "grey"), panel.grid.major=element_line(colour = "grey"), plot.margin=unit(c(0.2,0.2,0.2,0.2), "cm"), axis.ticks = element_line(size=1.5,colour="black"),axis.ticks.length=unit(0.2, "cm")) + 
  scale_x_continuous(breaks=0:18*1, limits=c(0.5,13.5),expand=c(0,0))+
  #scale_y_continuous(breaks=0:18*1,limits=c(0,13.8),expand=c(0,0))
  scale_y_continuous(breaks=0:18*1,limits=c(0,4.8),expand=c(0,0))

# Figure 4 b - Ver que version quiere Gustavo!! ###
Apo_vs_Holo <- read_delim("RMSD/Table_rmsd_Apo_vs_Holo.csv", ";", escape_double = FALSE, trim_ws = TRUE)

# Si seguimo el texto 
# "This tendency is also observed when we studied the correlation of global plDDT of the best model for each protein with the RMSDapo_holo (-0.45 Pearson correlation coefficient and P-value <0.001) (Figure 4b)"
# Hay que usar estas tablas:
models_vs_Apo_Holo_best <- read_delim("RMSD/Table_lower_RMSD_of_best_vs_ApoHolo.csv", ";", escape_double = FALSE, trim_ws = TRUE)
score_Global_best_model <- read_delim("RMSD/tabla_Score_Global_best_model.csv",";", escape_double = FALSE, trim_ws = TRUE)

ggplot() + 
  geom_point(aes(x = Apo_vs_Holo$RMSD, y = score_Global_best_model$score),color="black",shape=15,size=4,alpha=0.7) +
  labs(x=expression(RMSD[ApovsHolo]~(Å)),y=expression(plDDT[Score])) + 
  theme_bw() +
  theme(legend.position="none",text=element_text(family="Times New Roman") , axis.title.y = element_text(size = 54,vjust=1.0), axis.text.y = element_text(size = 44,colour="black"), axis.title.x = element_text(size = 54,vjust=1.0), axis.text.x = element_text(size = 44,colour="black"), panel.grid.minor=element_line(colour = "dark grey"), panel.grid.major=element_line(colour = "dark grey"), plot.margin=unit(c(0.8,0.6,0.2,0.2), "cm"), axis.ticks = element_line(size=1.5,colour="black"),axis.ticks.length=unit(0.2, "cm")) +
  geom_smooth(aes(x = Apo_vs_Holo$RMSD, y = score_Global_best_model$score),method = "lm", color="red") +
  scale_x_continuous(limits=c(0,15.2),expand=c(0,0))+
  scale_y_continuous(limits=c(75,100.0),expand=c(0,0))

# Si tenemos que usar los datos de score de los "lower RMSD models" hay que usar:
models_vs_Apo_Holo_lower <- read_delim("RMSD/Table_lower_RMSD_of_all_vs_ApoHolo.csv", ";", escape_double = FALSE, trim_ws = TRUE)
score_Global_lower_model <- read_delim("RMSD/tabla_Score_Global_lower_model.csv",";", escape_double = FALSE, trim_ws = TRUE)

ggplot() + 
  geom_point(aes(x = Apo_vs_Holo$RMSD, y = score_Global_lower_model$score),color="black",shape=15,size=4,alpha=0.7) +
  labs(x=expression(RMSD[ApovsHolo]~(Å)),y=expression(plDDT[Score])) + 
  theme_bw() +
  theme(legend.position="none",text=element_text(family="Times New Roman") , axis.title.y = element_text(size = 54,vjust=1.0), axis.text.y = element_text(size = 44,colour="black"), axis.title.x = element_text(size = 54,vjust=1.0), axis.text.x = element_text(size = 44,colour="black"), panel.grid.minor=element_line(colour = "dark grey"), panel.grid.major=element_line(colour = "dark grey"), plot.margin=unit(c(0.8,0.6,0.2,0.2), "cm"), axis.ticks = element_line(size=1.5,colour="black"),axis.ticks.length=unit(0.2, "cm")) +
  geom_smooth(aes(x = Apo_vs_Holo$RMSD, y = score_Global_lower_model$score),method = "lm", color="red") +
  scale_x_continuous(limits=c(0,15.2),expand=c(0,0))+
  scale_y_continuous(limits=c(75,100.0),expand=c(0,0))

# Supplementary Figure 1 
# Si consideramos todos los modelos
tabla_Score_Global <- read_delim("RMSD/tabla_Score_Global.csv",";", escape_double = FALSE, trim_ws = TRUE)
vec2 <- data.frame(x=tabla_Score_Global$score)
ggplot() + 
  geom_density(aes(x=x,y = ..count../sum(..count..)),color="black", fill="blue", alpha = 1.0, data=vec2) + 
  theme_bw()+
  labs(x="LDDT global score",y=expression(Density)) +
  theme( legend.position="none",text=element_text(family="Times New Roman") , axis.title.y = element_text(size = 54,vjust=1.0), axis.text.y = element_text(size = 44,colour="black"), axis.title.x = element_text(size = 54,vjust=1.0), axis.text.x = element_text(size = 44,colour="black"), panel.grid.minor=element_blank(), panel.grid.major=element_blank(), plot.margin=unit(c(0.4,0.2,0.2,0.2), "cm"), axis.ticks = element_line(size=1.5,colour="black"),axis.ticks.length=unit(0.2, "cm")) + 
  scale_x_continuous(limits=c(73,99.9),breaks=0:100*5, expand=c(0,0)) +
  scale_y_continuous(breaks=0:5*0.002,limits=c(0,0.0095),expand=c(0,0))

# Si consideramos solo los de best_score
tabla_Score_Global_best_model <- read_delim("RMSD/tabla_Score_Global_best_model.csv",";", escape_double = FALSE, trim_ws = TRUE)
vec2 <- data.frame(x=tabla_Score_Global_best_model$score)
ggplot() + 
  geom_density(aes(x=x,y = ..count../sum(..count..)),color="black", fill="blue", alpha = 1.0, data=vec2) + 
  theme_bw()+
  labs(x="LDDT global score",y=expression(Density)) +
  theme( legend.position="none",text=element_text(family="Times New Roman") , axis.title.y = element_text(size = 54,vjust=1.0), axis.text.y = element_text(size = 44,colour="black"), axis.title.x = element_text(size = 54,vjust=1.0), axis.text.x = element_text(size = 44,colour="black"), panel.grid.minor=element_blank(), panel.grid.major=element_blank(), plot.margin=unit(c(0.4,0.2,0.2,0.2), "cm"), axis.ticks = element_line(size=1.5,colour="black"),axis.ticks.length=unit(0.2, "cm")) +
  scale_x_continuous(limits=c(73,99.9),breaks=0:100*5, expand=c(0,0)) +
  scale_y_continuous(breaks=0:5*0.0025,limits=c(0,0.0115),expand=c(0,0))
