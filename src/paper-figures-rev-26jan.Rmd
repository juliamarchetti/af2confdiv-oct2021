---
title: "paper-figures-rev-26jan"
author: "Marchetti Julia"
date: "1/26/2022"
output: html_document
---

```{r, include=FALSE}
library(tidyverse)
library(patchwork)
library(GGally)
library(grid)
library(gridExtra)
library(gridtext)
library(ggpubr)
```

# Last result tables
```{r message=FALSE, warning=FALSE}
revision1_86_plus_5 <- read_delim("../data/26-01-2022/revision1_86_plus_5.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)
```

# Type of models 
```{r}
type_models <- revision1_86_plus_5 %>% 
  select(apo_id, holo_id, type, rmsd_apo_holo) %>% 
  rename(Apo_ID = apo_id, Holo_ID = holo_id)
```

# Remove proteins with high disorder content
```{r}
pdbs_to_remove_apo <- c('1NM4_A', '2KE0-15_A', '1JSP-15_B', '2KAX-5_A', '2QMV-9_A', '2CGK_B', '1CBI_A', '1JYU_A', '2MCG_2')
```


# Figure 1
### Plot
```{r message=FALSE, warning=FALSE}
# Read table
revision1_86_plus_5 <- read_delim("../data/26-01-2022/revision1_86_plus_5.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

revision1_86_plus_5 <- revision1_86_plus_5 %>% 
  filter(!apo_id %in% pdbs_to_remove_apo)

figure1 <-  
    ggplot() +
  
    # Capa de distribución general
    # geom_density(data = Apo_vs_Holo,
    #              aes(x = RMSD,y = ..count../sum(..count..)), 
    #              alpha = 0.3, color = 'orange',
    #              fill = 'orange',
    #              adjust = 1) + 
      
    geom_density(data = revision1_86_plus_5,
                 aes(x = rmsd_apo_holo), 
                 alpha = 0.3, color = 'black',
                 fill = 'black',
                 adjust = 1) + 
    xlab(expression(RMSD[Apo~vs~Holo]~(Å))) + ylab('Density') +
  
    scale_x_continuous(limits = c(0,15.5),breaks=seq(0, 15, 1), expand = c(0,0)) +
    scale_y_continuous(limits = c(0,0.25), breaks = seq(0.000, 0.25, 0.05), expand = c(0,0)) +
  
    theme(legend.position = "none") +
    
    theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
    theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
    theme(axis.title.x = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0))) +
  
    theme(axis.text=element_text(size = 10))+
    theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) 
```

### Statistics
```{r}
summary(revision1_86_plus_5$rmsd_apo_holo)
```


### Make figure 
```{r}
# Set size
png(res = 300, width = 7, height = 5, units = "in")

# Make plot
ggsave("../figures/revision/fig1.png", plot = figure1)
dev.off()

figure1
```

# Figure 2 

## Panel a - RSMD_apo vs RMSD_holo all models
### Plot
```{r}
Table_rmsd_vs_model_OK_95_plus_5 <- read_delim("../data/26-01-2022/Table_rmsd_vs_model-OK_95_plus_5.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  filter(!Apo_ID %in% pdbs_to_remove_apo) 

figure2a <- Table_rmsd_vs_model_OK_95_plus_5 %>% 
  ggplot(aes(x = RMSD_vs_Apo, y = RMSD_vs_Holo)) +
  geom_point(size = 4, show.legend = TRUE, alpha = 0.3,color = 'black', fill= 'black') +
  labs(x=expression(RMSD[~vs~Apo]~(Å)),y=expression(RMSD[~vs~Holo]~(Å)))  +
  
  geom_abline(intercept = 0, slope = 1,linetype="dashed", color= 'red') +
  scale_x_continuous(breaks=seq(0,15,2), limits=c(0,15),expand=c(0,0))+
  scale_y_continuous(breaks=seq(0,15,2),limits=c(0,15),expand=c(0,0)) +
  
  theme(axis.line = element_line(colour = "black"),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.border = element_blank(),
      panel.background = element_blank()) +
   
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 20))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  
  ggtitle('A')
  

rm(Table_rmsd_vs_model_OK_95_plus_5)
figure2a
```



## Panel b - RSMD_apo vs RMSD_holo best models
### Plot 
```{r}
figure2b <- revision1_86_plus_5 %>%  
    filter(!apo_id %in% pdbs_to_remove_apo) %>% 
    ggplot(aes(x = RMSD_best_score_md_Apo, y = RMSD_best_score_md_Holo)) + 
    geom_point(size= 4, show.legend = TRUE, alpha = 0.3,color = 'black', fill= 'black') +
    geom_abline(intercept = 0, slope = 1,linetype ="dashed", color = 'red') +

  
    labs(x=expression(RMSD[~vs~Apo]~(Å)),y = expression(RMSD[~vs~Holo]~(Å))) + 
    
    scale_x_continuous(breaks=seq(0,15,2), limits=c(0,15),expand=c(0,0))+
    scale_y_continuous(breaks=seq(0,15,2),limits=c(0,15),expand=c(0,0)) +
  
  
    theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank()) +
  
    theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
    theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
          axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
    theme(axis.text=element_text(size = 20))+
    theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
    theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  
    ggtitle('B')

figure2b
```

## Make figure 
```{r}
# Set size
png(res = 300, width = 14, height = 8, units = "in")

# Make plot 
figure2 <- figure2a + figure2b

# Save plot
ggsave("../figures/revision/fig2.png", plot = figure2)
dev.off()
```


# Figure 3 

## Table
```{r message=FALSE, warning=FALSE}
Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5 <- read_delim("../data/26-01-2022/Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  filter(!Apo_ID %in% pdbs_to_remove_apo) %>% 
  select(Apo_ID, Holo_ID,RMSD_prom_models_vs_Apo, RMSD_prom_models_vs_Holo, lowest_RMSD_vs_Apo_Holo)

table_fig3 <- Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5 %>% 
  left_join(type_models) %>% 
  mutate(type_facet = ifelse(type == 'Holo', 'Proteins closer to Holo', 'Proteins closer to Apo')) %>% 
  pivot_longer(cols = c("RMSD_prom_models_vs_Apo", "RMSD_prom_models_vs_Holo"), 
               names_to = "RMSD_prom_apo_holo", # nuevo nombre de la columna
               values_to = "RMSD")  %>%  # no me convence el nombre
  mutate(RMSD_prom_apo_holo = ifelse(RMSD_prom_apo_holo == 'RMSD_prom_models_vs_Apo', 'to Apo', 'to Holo')) 

rm(Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5)
```


## Plot
```{r}
compare_pairs <- list(c("to Holo", "to Apo"))
figure3 <- table_fig3 %>%   
  
    ggboxplot(x = 'RMSD_prom_apo_holo', y = "RMSD", facet.by = "type_facet", 
             palette = c("orange","green"),
             fill = 'RMSD_prom_apo_holo')+
    
    stat_compare_means(comparisons = compare_pairs,label = "p.format", method = "wilcox.test") +
  
    scale_y_continuous(breaks=0:18*2,limits =c(0,15),expand=c(0,0))+
    
    xlab('') + ylab(expression(RMSD~(Å))) +
    theme(legend.position = '')+
    theme(axis.title.x = element_text(face ='plain',size = 20, margin = margin(t = 10, r = 8, b = 0, l = 0)),
          axis.title.y = element_text(face ='plain',size = 20, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
    theme(axis.text=element_text(size = 13))+
    theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
    theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
    theme(strip.text.x = element_text(size = 13))
figure3
```

## Make figure
```{r}
# Set size
png(res = 300, width = 10, height = 5, units = "in")

# Save plot
ggsave("../figures/revision/fig3.png", plot = figure3)
dev.off()

rm(table_fig3)
```


## Statistics 
```{r}
Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5 <- read_delim("../data/26-01-2022/Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>% 
  filter(!Apo_ID %in% pdbs_to_remove_apo) %>% 
  select(Apo_ID, Holo_ID,RMSD_prom_models_vs_Apo, RMSD_prom_models_vs_Holo, lowest_RMSD_vs_Apo_Holo)

table_fig3 <- Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5 %>% 
  left_join(type_models) 
```

```{r}
# Mean RMSD when type is Apo
table_fig3 %>% 
  filter(type == 'Apo') %>% 
  summarise(mean(RMSD_prom_models_vs_Apo),  mean(RMSD_prom_models_vs_Holo))

# Mean RMSD when type is Holo
table_fig3 %>% 
  filter(type == 'Holo') %>% 
  summarise(mean(RMSD_prom_models_vs_Apo),  mean(RMSD_prom_models_vs_Holo))
```

```{r}
# only best to apo, keep average of unfavored form (holo)
best_to_apo <- table_fig3 %>% 
  filter(type == 'Apo') %>% 
  select(rmsd_apo_holo, RMSD_prom_models_vs_Holo)
cor.test(best_to_apo$RMSD_prom_models_vs_Holo, best_to_apo$rmsd_apo_holo)
```

```{r}
# only best to holo, keep average of unfavored form (apo)
best_to_holo <- table_fig3 %>% 
  filter(type == 'Holo') %>% 
  select(rmsd_apo_holo, RMSD_prom_models_vs_Apo)
cor.test(best_to_holo$RMSD_prom_models_vs_Apo, best_to_holo$rmsd_apo_holo)
```

```{r}
best_to_apo <- best_to_apo %>%  
  rename(RMSD_prom_unfavored_form = RMSD_prom_models_vs_Holo)

best_to_holo <- best_to_holo %>%  
  rename(RMSD_prom_unfavored_form = RMSD_prom_models_vs_Apo)

best_to_model <- rbind(best_to_apo, best_to_holo)

cor.test(best_to_model$rmsd_apo_holo, best_to_model$RMSD_prom_unfavored_form)
```


# Figure S3  - alternative with average (not lowest)

### Plot
```{r}
figS3 <-
  ggplot(best_to_model)+
  geom_point(aes(x=RMSD_prom_unfavored_form, y=rmsd_apo_holo), size=4, alpha=0.3)+
  geom_smooth(aes(x = RMSD_prom_unfavored_form, y = rmsd_apo_holo),method = "lm", color="blue") +

  ylab(expression(RMSD[Apo~vs~Holo]~(Å))) +
  xlab(expression(RMSD~against~unfavored~form~(Å))) +
  
  scale_x_continuous(breaks=seq(1,15,1), limits=c(0,16),expand=c(0,0))+
  scale_y_continuous(breaks=seq(1,15,1), limits=c(0,16),expand=c(0,0))+
  
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank()) +
   
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 20, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 20))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))
figS3
```

### Make figure
```{r}
# Set size
png(res = 300, width = 12, height = 8, units = "in")

# Save plot
ggsave("../figures/revision/figS3.png", plot = figS3)
dev.off()
```

### rm tables
```{r}
rm(Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5, best_to_apo, best_to_holo, best_to_model)
```


# Figure 5

### Panel A - lowest RMSD (y) vs RMSD apo holo (x)

#### Plot
```{r}
figure5a <- revision1_86_plus_5 %>%  
  filter(!apo_id %in% pdbs_to_remove_apo) %>% 
  
  ggplot(aes(x = rmsd_apo_holo, y = lowest_rmsd_md_apo_or_holo))+
  geom_point(fill= 'black', color= 'black', alpha=0.3, size = 4) +
  geom_smooth(aes(x = rmsd_apo_holo, y = lowest_rmsd_md_apo_or_holo),method = "lm", color="blue") +


  labs(x=expression(RMSD[Apo~vs~Holo]~(Å)),y=expression(lowest~RMSD~(Å))) + 
  scale_x_continuous(breaks=0:18*1, limits=c(0,15.5),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*1,limits=c(0,14),expand=c(0,0))+
      
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank()) +
  
  theme(legend.position = 'none') +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 20))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(plot.title = element_text(face='bold'))+
  ggtitle('A') 
figure5a
```

#### Statistics
```{r}
cor.test(revision1_86_plus_5$rmsd_apo_holo, revision1_86_plus_5$lowest_rmsd_md_apo_or_holo,
         method = 'pearson')
```

### Panel B - figure score best model (y) vs rmsd apo holo (x) 
#### Plot
```{r}
figure5b <- revision1_86_plus_5 %>%  
  filter(!apo_id %in% pdbs_to_remove_apo) %>%  
  
  ggplot()+
  geom_point(aes(x = rmsd_apo_holo, y = mds_best_global_score), color = 'black', fill= 'black',  alpha=0.3, size = 4)+
  geom_smooth(aes(x = rmsd_apo_holo, y = mds_best_global_score),method = "lm", color="blue") +
  
  labs(x=expression(RMSD[Apo~vs~Holo]~(Å)),y=expression(plDDT[Score])) + 
  
  scale_x_continuous(limits=c(0.5,15.5), breaks=0:18*1,expand=c(0,0))+
  scale_y_continuous(limits=c(70,100.0), breaks = seq(from = 70, to = 100, by = 5), expand=c(0,0))+

  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank()) +
  
  theme(legend.position = 'none') +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 20))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(plot.title = element_text(face='bold'))+
  ggtitle('B') 
figure5b
```

#### Statistics
```{r}
cor.test(revision1_86_plus_5$rmsd_apo_holo, revision1_86_plus_5$mds_best_global_score,
         method = 'pearson')
```
```{r}
# model error vs length 
cor.test(revision1_86_plus_5$lowest_rmsd_md_apo_or_holo, revision1_86_plus_5$length,
         method = 'pearson')
```
```{r}
# model error vs seqs aligned  
cor.test(revision1_86_plus_5$lowest_rmsd_md_apo_or_holo, revision1_86_plus_5$seqs_aligned,
         method = 'pearson')
```

```{r}
# model error vs neff  
cor.test(revision1_86_plus_5$lowest_rmsd_md_apo_or_holo, revision1_86_plus_5$neff,
         method = 'pearson')
```

```{r}
# model error as function of motion type 

loop <- subset(revision1_86_plus_5, revision1_86_plus_5$motion_type == 'loop movements')
domain <- subset(revision1_86_plus_5, revision1_86_plus_5$motion_type == 'domain movements') 

wilcox.test(loop$lowest_rmsd_md_apo_or_holo, domain$lowest_rmsd_md_apo_or_holo)

median(loop$lowest_rmsd_md_apo_or_holo)
median(domain$lowest_rmsd_md_apo_or_holo)

rm(loop, domain)
```


## Make figure
```{r}
# Set size
png(res = 300, width = 14, height = 8, units = "in")

# Make plot 
figure5 <- figure5a + figure5b
figure5

# Save plot
ggsave("../figures/revision/fig5.png", plot = figure5)
dev.off()
```


# Figure 6 - rmsd clusters (ex-figure5)
### Read tables
```{r message=FALSE, warning=FALSE}
clusters_maxRMSD_stats_Xray_ <- read_delim("../data/26-01-2022/clusters_maxRMSD_stats(Xray).csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

clusters <- read_delim("../data/25-10-2021/clusters.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)
clusters <- clusters %>% 
  mutate(Disperse = ifelse(`Dispersa (Sigma>2)` == 1,'heterogeneus','homogeneus')) %>% 
  mutate(Rigid = ifelse(`Rigida (RMSD<1.0)` == 1,'rigid', 'not rigid'))

X196_lowestRmsd_with_method_maxs <- read_delim("../data/26-01-2022/196_lowestRmsd_with_method_maxs.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

X196_lowestRmsd_with_method_maxs <- X196_lowestRmsd_with_method_maxs %>% 
  mutate(cluster = paste('Clus',Cluster_ID, sep = "")) %>% 
  filter(methods == 'X-RAY DIFFRACTION..X-RAY DIFFRACTION')
```

### Define categories (rigid/dispersion)
```{r}
clusters_maxRMSD_stats_Xray_ <- clusters_maxRMSD_stats_Xray_ %>% 
  mutate(rango = abs(max-min)) %>% 
  mutate(dispersion_range = ifelse(rango >= 4, 'heterogeneous', 'homogeneous')) %>% 
  mutate(dispersion = ifelse(std >2, 'heterogeneous', 'homogeneous')) %>% 
  mutate(rigida = ifelse(mean < 1, 'rigid', 'flexible')) %>% 
  filter(count >= 8) 
```


### Merge two tables
```{r}
figure6_table <- X196_lowestRmsd_with_method_maxs %>% 
  left_join(clusters_maxRMSD_stats_Xray_)
```

### Panel a -- dispersion
#### Plot
```{r}
compare_pairs <- list(c('heterogeneous','homogeneous'))

figure6a <- figure6_table %>% 
  drop_na(dispersion_range) %>% 
  ggboxplot(x = 'dispersion_range', y = "RMSD_Mammoth")+
  stat_compare_means(comparisons = compare_pairs,label = "p.format", method = "wilcox.test", size = 5)+

  xlab('') + ylab(expression(lowest~RMSD~(Å)))+
  ggtitle('A')+
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(axis.title.x = element_text(face ='plain',size = 20),
        axis.title.y = element_text(face ='plain',size = 20))+
  theme(axis.text=element_text(size = 20))+
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))
figure6a
```

#### Statistics
```{r}
homo <- figure6_table %>% filter(dispersion_range == 'homogeneous')
hetero <- figure6_table %>% filter(dispersion_range == 'heterogeneous')

summary(homo$RMSD_Mammoth)
summary(hetero$RMSD_Mammoth)

wilcox.test(homo$RMSD_Mammoth, hetero$RMSD_Mammoth)
rm(homo, hetero)
```


### Panel B -- rigid and flexible
#### Plot
```{r}
compare_pairs <- list(c('rigid','flexible'))

figure6b <-figure6_table %>% 
  drop_na(rigida) %>% 
  ggboxplot(x = 'rigida', y = "RMSD_Mammoth")+
  stat_compare_means(comparisons = compare_pairs,label = "p.format", method = "wilcox.test", size = 5)+

  xlab('') + ylab(expression(lowest~RMSD~(Å)))+
  ggtitle('B')+
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(axis.title.x = element_text(face ='plain',size = 20),
        axis.title.y = element_text(face ='plain',size = 20))+
  theme(axis.text=element_text(size = 20))+
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))
figure6b
```

#### Statistics
```{r}
rigid <- figure6_table %>% filter(rigida == 'rigid')
flexible <- figure6_table %>% filter(rigida == 'flexible')

summary(rigid$RMSD_Mammoth)
summary(flexible$RMSD_Mammoth)

wilcox.test(rigid$RMSD_Mammoth, flexible$RMSD_Mammoth)
rm(rigid, flexible)
```

### Make figure
```{r}
# Set size
png(res = 300, width = 12, height = 6, units = "in")

# Make plot 
figure6 <- figure6a + figure6b
figure6

# Save plot
ggsave("../figures/revision/fig6.png", plot = figure6)
dev.off()

rm(X196_lowestRmsd_with_method_maxs)
rm(clusters)
rm(clusters_maxRMSD_stats_Xray_)
rm(figure6_table)
```



# Figure 7 

### Left panel Lowest RMSD - all residues

#### Plot
```{r message=FALSE, warning=FALSE}
Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5 <- read_delim("../data/26-01-2022/Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE) %>%  
  filter(!Apo_ID%in% pdbs_to_remove_apo) 


Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5<-Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5 %>% 
  mutate(Score_Lower_rmsd_M_factor= case_when(
                   Score_Lower_rmsd_M == 0 | Score_Lower_rmsd_M < 50 ~ "[0-50)",
                   Score_Lower_rmsd_M >= 50 & Score_Lower_rmsd_M < 70 ~ "[50-70)",
                   Score_Lower_rmsd_M >= 70 & Score_Lower_rmsd_M < 90 ~ "[70-90)",
                   Score_Lower_rmsd_M >= 90 & Score_Lower_rmsd_M <= 100 ~ "[90-100)")) %>% 
  mutate(Score_Lower_rmsd_M_factor = as.factor(Score_Lower_rmsd_M_factor))
  levels(as.factor(Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5$Score_Lower_rmsd_M_factor))  


figure7_left_boxplot <- Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5 %>% 
  ggboxplot(x = "Score_Lower_rmsd_M_factor", y = "RMSF_CA",
            title = '')+
  labs(y=expression(RMSF~C[alpha]~(Å)),x=expression(plDDT[Score])) +
  scale_y_continuous(breaks=seq(0,40,10),limits=c(0,45),expand=c(0,0))+

    theme(axis.line = element_line(colour = "black"),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.border = element_blank(),
          panel.background = element_blank()) +
    
    theme(panel.grid.major.y = element_blank(),
          panel.grid.major.x = element_blank()) +
  
    theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
    theme(axis.text=element_text(size = 20))+
    theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
    theme(plot.title = element_text(face='bold', vjust=0, size = 20)) +
    theme(plot.title = element_text(face='bold'))+
    
  ggtitle('A')
figure7_left_boxplot
```

#### Statistics
```{r}
cor.test(Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5$RMSF_CA, 
          Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5$Score_Lower_rmsd_M)
rm(Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5)
```

### Right panel  Lowest RMSD - every 15 residues

#### Plot
```{r}
lowest_rmsd_window_15_86proteins_plus_5 <- read_delim("../data/26-01-2022/lowest_rmsd_window_15_86proteins_plus_5.csv",  ";", escape_double = FALSE, trim_ws = TRUE)

lowest_rmsd_window_15_86proteins_plus_5 <- lowest_rmsd_window_15_86proteins_plus_5 %>% 
  mutate(Score_Lower_rmsd_M_factor= case_when(
                   scores == 0 | scores < 50 ~ "[0-50)",
                   scores >= 50 & scores < 70 ~ "[50-70)",
                   scores >= 70 & scores < 90 ~ "[70-90)",
                   scores >= 90 & scores <= 100 ~ "[90-100)")) %>% 
  mutate(Score_Lower_rmsd_M_factor = as.factor(Score_Lower_rmsd_M_factor))
levels(as.factor(lowest_rmsd_window_15_86proteins_plus_5$Score_Lower_rmsd_M_factor))  

figure7_right_boxplot <- 
  lowest_rmsd_window_15_86proteins_plus_5 %>% 
  ggboxplot(x = "Score_Lower_rmsd_M_factor", y = "rmsf",
            title = '')+
  labs(y=expression(RMSF~C[alpha]~(Å)),x=expression(plDDT[Score])) +
  scale_y_continuous(breaks=seq(0,40,10),limits=c(0,45),expand=c(0,0))+

    theme(axis.line = element_line(colour = "black"),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.border = element_blank(),
          panel.background = element_blank()) +
    
    theme(panel.grid.major.y = element_blank(),
          panel.grid.major.x = element_blank()) +
  
    theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
    theme(axis.text=element_text(size = 20))+
    theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
    theme(plot.title = element_text(face='bold', vjust=0, size = 20)) +
    theme(plot.title = element_text(face='bold'))+
    
  ggtitle('B')

figure7_right_boxplot
```


#### Statistics

```{r}
cor.test(lowest_rmsd_window_15_86proteins_plus_5$rmsf, 
          lowest_rmsd_window_15_86proteins_plus_5$scores)
rm(lowest_rmsd_window_15_86proteins_plus_5)
```

### Make plot fig 7
```{r}
# Set size
png(res = 300, width = 15, height = 7, units = "in")

# make plot 
figure7_boxplot <- figure7_left_boxplot + figure7_right_boxplot

# Save plot
ggsave("../figures/revision/fig7_boxplot.png", plot = figure7_boxplot)
dev.off()
```

# Figure S1 

## Panel A - Distribution Global Score (All)

### Plot 
```{r message=FALSE, warning=FALSE}

tabla_Score_Global_86_plus_5 <- read_delim("../data/26-01-2022/tabla_Score_Global_86_plus_5.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)  

figureS1_a <- tabla_Score_Global_86_plus_5 %>% 
  
    ggplot() +
    geom_density(aes(x = score), color = 'black', fill = 'black', 
                alpha = 0.3,
                adjust = 1) +
    labs(x=expression(plDDT[Score]),y=expression(Density))+
  
    scale_x_continuous(limits=c(0,101), breaks = seq(0,100,10),expand=c(0,0))+
    theme(axis.line = element_line(colour = "black"),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.border = element_blank(),
          panel.background = element_blank()) +
    
    theme(panel.grid.major.y = element_blank(),
          panel.grid.major.x = element_blank()) +
    
    theme(axis.title.x = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)),
          axis.title.y = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
    theme(axis.text=element_text(size = 10))+
    theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
    theme(plot.title = element_text(face='bold'))+
    
    ggtitle('A')


figureS1_a
rm(tabla_Score_Global_86_plus_5)
```


## Panel B - only score best models
### Plot

```{r}
figureS1_b <- revision1_86_plus_5 %>% 
    ggplot() +
    geom_density(aes(x = mds_best_global_score),color = 'black', fill = 'black', 
                alpha = 0.3,
                adjust = 1) +
  
    labs(x=expression(plDDT[Score]),y=expression(Density))+
  
    scale_x_continuous(limits = c(0,101),breaks=seq(0, 100, 10), expand = c(0,0)) +
  
    theme(axis.line = element_line(colour = "black"),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.border = element_blank(),
          panel.background = element_blank()) +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  
  theme(axis.title.x = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 13, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 10))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold'))+
  ggtitle('B')

figureS1_b
```

## Make figure 

```{r}
# Set size
png(res = 300, width = 7, height = 9, units = "in")

# Make plot
figureS1 <- figureS1_a / figureS1_b

# Save plot
ggsave("../figures/revision/figS1.png", plot = figureS1)
dev.off()
```

# Figure S2

## Panel A - lowest_rmsd_md_apo_or_holo < 4
### Plot
```{r}
figS2_a <- revision1_86_plus_5 %>% 
  filter(!apo_id %in% pdbs_to_remove_apo) %>% 
  filter(lowest_rmsd_md_apo_or_holo < 4) %>% 
  
  ggplot(aes(x = rmsd_apo_holo, y = lowest_rmsd_md_apo_or_holo))+
  geom_point(fill= 'black', color= 'black', alpha=0.3, size = 4) +
  geom_smooth(aes(x = rmsd_apo_holo, y = lowest_rmsd_md_apo_or_holo),method = "lm", color="blue") +
  
  labs(x=expression(RMSD[Apo~vs~Holo]~(Å)),y=expression(lowest~RMSD~(Å))) + 
  scale_x_continuous(breaks=0:18*1, limits=c(0,15.5),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*1,limits=c(0,4.5),expand=c(0,0))+
  
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank()) +
  
  theme(legend.position = 'none') +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 20))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(plot.title = element_text(face='bold'))+
  ggtitle('A') 

figS2_a  
```

### Statistics
```{r}
temp <- revision1_86_plus_5 %>% 
  filter(!apo_id %in% pdbs_to_remove_apo) %>% 
  filter(lowest_rmsd_md_apo_or_holo < 4) 
  
cor.test(temp$rmsd_apo_holo, temp$lowest_rmsd_md_apo_or_holo, method = 'pearson')
rm(temp)
```



## Panel B - lowest_rmsd_md_apo_or_holo < 3
### Plot
```{r}
figS2_b <- revision1_86_plus_5 %>% 
  filter(!apo_id %in% pdbs_to_remove_apo) %>% 
  filter(lowest_rmsd_md_apo_or_holo < 3) %>% 
  
  ggplot(aes(x = rmsd_apo_holo, y = lowest_rmsd_md_apo_or_holo))+
  geom_point(fill= 'black', color= 'black', alpha=0.3, size = 4) +
  geom_smooth(aes(x = rmsd_apo_holo, y = lowest_rmsd_md_apo_or_holo),method = "lm", color="blue") +
  
  labs(x=expression(RMSD[Apo~vs~Holo]~(Å)),y=expression(lowest~RMSD~(Å))) + 
  scale_x_continuous(breaks=0:18*1, limits=c(0,15.5),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*1,limits=c(0,4.5),expand=c(0,0))+
  
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank()) +
  
  theme(legend.position = 'none') +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 20))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(plot.title = element_text(face='bold'))+
  ggtitle('B') 

figS2_b
```

## Statistics
```{r}
temp <- revision1_86_plus_5 %>% 
  filter(!apo_id %in% pdbs_to_remove_apo) %>% 
  filter(lowest_rmsd_md_apo_or_holo < 3) 
  
cor.test(temp$rmsd_apo_holo, temp$lowest_rmsd_md_apo_or_holo, method = 'pearson')
rm(temp)
```

## Make figure 
```{r}
# Set size
png(res = 300, width = 14, height = 8, units = "in")

# Make plot 
figureS2 <- figS2_a + figS2_b
figureS2

# Save plot
ggsave("../figures/revision/figS2.png", plot = figureS2)
dev.off()
```


# Figures to answer

## Fig S4

### make table 
```{r message=FALSE, warning=FALSE}
Table_degree_prom_86_plus_5 <- read_delim("../data/26-01-2022/Table_degree_prom_86_plus_5.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

table_contact <- Table_degree_prom_86_plus_5 %>% 
   pivot_longer(cols = c("Apo_degree_prom", "Holo_degree_prom"), 
               names_to = "Form", # nuevo nombre de la columna
               values_to = "Average number of inter-residue contacts") %>% 
   mutate(Form = ifelse(Form == 'Apo_degree_prom', 'Apo', 'Holo'))

rm(Table_degree_prom_86_plus_5)
```



### Plot
```{r}
compare_pairs <- list(c("Apo", "Holo"))
figS4 <- table_contact %>%   
  
  ggboxplot(x = 'Form', y = "Average number of inter-residue contacts", 
           palette = c("orange","green"),
           fill = 'Form')+
  
  stat_compare_means(comparisons = compare_pairs,label = "p.format", method = "wilcox.test") +


  ylab("Average number of inter-residue contacts") + xlab('')+
  theme(legend.position = '')+
  theme(axis.text=element_text(size = 13))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(strip.text.x = element_text(size = 13))

figS4
```

### Statistics
```{r}
table_contact %>% 
  group_by(Form) %>% 
  summarise(median(`Average number of inter-residue contacts`), 
            mean(`Average number of inter-residue contacts`))
```

### Make figure
```{r}
# Set size
png(res = 300, width = 7, height = 5, units = "in")

# Save plot
ggsave("../figures/revision/figS4.png", plot = figS4)
dev.off()

rm(table_contact)
```


## Fig S5 Rg figure

### Make table 
```{r message=FALSE, warning=FALSE}
Table_Rg_data_set_5_2022 <- read_delim("../data/26-01-2022/Table_Rg_data_set_5_2022.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

Table_Rg_data_set_86 <- read_delim("../data/26-01-2022/Table_Rg_data_set_86.csv", 
    ";", escape_double = FALSE, trim_ws = TRUE)

table_rg <- rbind(Table_Rg_data_set_5_2022, Table_Rg_data_set_86)
rm(Table_Rg_data_set_5_2022, Table_Rg_data_set_86)

table_rg <- table_rg %>% 
   pivot_longer(cols = c("RG_Apo", "RG_Holo"), 
               names_to = "Form", # nuevo nombre de la columna
               values_to = "RG") %>% 
   mutate(Form = ifelse(Form == 'RG_Apo', 'Apo', 'Holo'))
```


### Plot
```{r}
compare_pairs <- list(c("Apo", "Holo"))
figS5 <- table_rg %>%   
  
  ggboxplot(x = 'Form', y = "RG", 
           palette = c("orange","green"),
           fill = 'Form')+
  
  stat_compare_means(comparisons = compare_pairs,label = "p.format", method = "wilcox.test") +
  xlab('') + 
  theme(legend.position = '')+
  theme(axis.text=element_text(size = 13))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(strip.text.x = element_text(size = 13))

figS5
```

### Statistics
```{r}
table_rg %>% 
  group_by(Form) %>% 
  summarise(median(RG), mean(RG))
```
 
### Make figure
```{r}
# Set size
png(res = 300, width = 7, height = 5, units = "in")

# Save plot
ggsave("../figures/revision/figS5.png", plot = figS5)
dev.off()

rm(table_rg)
```

# Figure 5 - answer file

```{r}
new_pdbs <- c('7AZP_A', '4LP5_A', '1CFC_A', '2KQ2-6_A', '1DMO-18_A')
highlight_df <- revision1_86_plus_5 %>% 
  filter(apo_id %in% new_pdbs)
rm(new_pdbs)
```

### Panel A - lowest RMSD (y) vs RMSD apo holo (x)
#### Plot
```{r}
figure5a_answer <- revision1_86_plus_5 %>%  
  filter(!apo_id %in% pdbs_to_remove_apo) %>% 
  
  ggplot(aes(x = rmsd_apo_holo, y = lowest_rmsd_md_apo_or_holo))+
  geom_point(fill= 'black', color= 'black', alpha=0.3, size = 4) +
  geom_point(data=highlight_df, aes(x=rmsd_apo_holo,y=lowest_rmsd_md_apo_or_holo), color='red',size=4, alpha=0.3)+
  
  geom_smooth(aes(x = rmsd_apo_holo, y = lowest_rmsd_md_apo_or_holo),method = "lm", color="blue") +

  labs(x=expression(RMSD[Apo~vs~Holo]~(Å)),y=expression(lowest~RMSD~(Å))) + 
  scale_x_continuous(breaks=0:18*1, limits=c(0,15.5),expand=c(0,0))+
  scale_y_continuous(breaks=0:18*1,limits=c(0,14),expand=c(0,0))+
      
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank()) +
  
  theme(legend.position = 'none') +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  
  theme(axis.text=element_text(size = 20))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(plot.title = element_text(face='bold'))+
  ggtitle('A') 

figure5a_answer
```

### Panel B - figure score best model (y) vs rmsd apo holo (x) 
#### Plot
```{r}
figure5b_answer <- revision1_86_plus_5 %>%  
  filter(!apo_id %in% pdbs_to_remove_apo) %>%  
  
  ggplot()+
  geom_point(aes(x = rmsd_apo_holo, y = mds_best_global_score), color = 'black', fill= 'black',  alpha=0.3, size = 4)+
  geom_smooth(aes(x = rmsd_apo_holo, y = mds_best_global_score),method = "lm", color="blue") +
    geom_point(data=highlight_df, aes(x=rmsd_apo_holo,y=mds_best_global_score), color='red',size=4, alpha=0.3)+
  labs(x=expression(RMSD[Apo~vs~Holo]~(Å)),y=expression(plDDT[Score])) + 
  
  scale_x_continuous(limits=c(0.5,15.5), breaks=0:18*1,expand=c(0,0))+
  scale_y_continuous(limits=c(70,100.0), breaks = seq(from = 70, to = 100, by = 5), expand=c(0,0))+

  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank()) +
  
  theme(legend.position = 'none') +
  
  theme(panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_blank()) +
  
  theme(axis.title.x = element_text(face ='plain',size = 25, margin = margin(t = 10, r = 8, b = 0, l = 0)),
        axis.title.y = element_text(face ='plain',size = 25, margin = margin(t = 0, r = 8, b = 0, l = 0)))+
  theme(axis.text=element_text(size = 20))+
  theme(plot.margin = unit(c(.2,.2,.2,.2), "cm")) +
  theme(plot.title = element_text(face='bold', vjust=0, size = 20))+
  theme(plot.title = element_text(face='bold'))+
  ggtitle('B') 
  
figure5b_answer
```

## Make figure
```{r}
# Set size
png(res = 300, width = 14, height = 8, units = "in")

# Make plot 
figure5_answer <- figure5a_answer + figure5b_answer
figure5_answer

# Save plot
ggsave("../figures/revision/fig5_answer.png", plot = figure5_answer)
dev.off()
rm(highlight_df)
```

### Check statistics from text fig2

```{r}
table_fig2 <- revision1_86_plus_5 %>% 
  select(RMSD_best_score_md_Apo, RMSD_best_score_md_Holo, apo_id) %>% 
  group_by(apo_id) %>% 
  mutate(min_rmsd_apo_or_holo = min(RMSD_best_score_md_Apo, RMSD_best_score_md_Holo)) %>% 
  mutate(max_rmsd_apo_or_holo = max(RMSD_best_score_md_Apo, RMSD_best_score_md_Holo)) %>% 
  ungroup() %>% 
  mutate(min_rmsd_range = 0.05 * min_rmsd_apo_or_holo + min_rmsd_apo_or_holo) %>% 
  mutate(is_apo_similar_holo = ifelse(max_rmsd_apo_or_holo < min_rmsd_range,1,0)) 

table_fig2 %>%  
  group_by(is_apo_similar_holo) %>% 
  count(n())

rm(table_fig2)
```

